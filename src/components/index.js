import Vue from "vue";

//blocks
import AppProject from "@/components/blocks/AppProjectComponent";
import AppGroup from "@/components/blocks/AppGroupComponent";

Vue.component("app-project", AppProject);
Vue.component("app-group", AppGroup);


//modals
import AppModalCreate from "@/components/modals/AppModalCreateProjectComponent";

Vue.component("app-modal-create", AppModalCreate);


//fields
import AppInputComponent from "@/components/fields/AppInputComponent";

Vue.component("app-input", AppInputComponent);
