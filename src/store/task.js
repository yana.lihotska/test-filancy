const SET_ITEMS = "SET_ITEMS";
const CREATE_ITEM = "CREATE_ITEM";
const EDIT_ITEM = "EDIT_ITEM";
const EDIT_ITEMS = "EDIT_ITEMS";

export const state = () => ({
  items: [],
});

export const getters = {
  items: (state) => state.items,
};

export const actions = {
  async getTasks({ commit }, filters) {
    let res = require("@/static/api/data.json");
    commit("SET_ITEMS", res);
    return res;
  },
  create({ commit }, payload) {
    commit("CREATE_ITEM", payload);
  },
  edit({ commit }, payload) {
    commit("EDIT_ITEM", payload);
  },
  editGroups({ commit }, payload) {
    commit("EDIT_ITEMS", payload);
  },
};

export const mutations = {
  [SET_ITEMS](state, payload) {
    state.items = payload;
  },
  [CREATE_ITEM](state, payload) {
    let index = _.findIndex(state.items, (el) => el.id === payload.id);
    state.items[index].projects.push(payload.project);
  },
  [EDIT_ITEM](state, payload) {
    state.items = _.forEach(
      state.items, (value) => {
        _.forEach(value.projects, (el) => {
          if(el.id ===  payload.id) {
            el.name = payload.name;
          }
        })
      })

  },
  [EDIT_ITEMS](state, payload) {
    state.items = payload
  },
};
