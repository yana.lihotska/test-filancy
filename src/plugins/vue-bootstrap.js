import Vue from 'vue'
import {ModalPlugin, ToastPlugin, BAlert, BButton} from 'bootstrap-vue'

Vue.component('BAlert', BAlert)
Vue.component('BButton', BButton)

Vue.use(ModalPlugin)
Vue.use(ToastPlugin)
