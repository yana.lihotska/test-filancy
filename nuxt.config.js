  require('dotenv').config();
module.exports = {
  ssr: false,
  generate: {
    fallback: false,
    minify: false,
  },
  router: {
    prefetchLinks: process.env.NUXT_APP_MODE === 'production',
    middleware: 'router-auth'
  },
  buildModules: [
    '@nuxtjs/moment'
  ],
  moment: {
    defaultLocale: 'ru',
    locales: ['ru'],
  },
  plugins: [
    '~/components',
    { src: "~plugins/vue-bootstrap", ssr: false },
    { src: '~plugins/vuedraggable', ssr: false }
  ],
  modules: [
    ['@nuxtjs/dotenv', { path: '', systemvars: true }],
     ['cookie-universal-nuxt', { alias: 'cookies' }],
  ],
  css: ['~/assets/scss/common.scss'],
  srcDir: 'src',
  /*
  ** Headers of the page
  */
  head: {
    title: 'filancy',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

